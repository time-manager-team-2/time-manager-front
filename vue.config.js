// vue.config.js
const webpack = require('webpack');

// Jquery and Raphael for vue-morris
module.exports = {
  configureWebpack: {
    plugins: [
      new webpack.ProvidePlugin({
        $: 'jquery',
        jQuery: 'jquery',
        Raphael: 'raphael'
      })
    ]
  }
};