# time-manager

## Project setup
```
npm install
```

> Work with backend project available [here](https://gitlab.com/time-manager-team-2/time-manager-api)

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

## Docker setup

see project CI [here](https://gitlab.com/time-manager-team-2/time-manager-ci) for more informations


### Created by

 * Sylvain Dupuy
 * Michaël Lucas 
 * David Schoffit
 * Said Mammar
