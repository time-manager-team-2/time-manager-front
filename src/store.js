import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import {ADMIN_ID, MANAGER_ID, EMPLOYEE_ID} from './roles.js'
import moment from 'moment'

Vue.use(Vuex)

function addOneDay(date) {
  var result = new Date(date)
  result.setDate(result.getDate() + 1)
  return moment(result).startOf('day').toDate()
}

function getDates(startDate, stopDate) {
  var dateArray = new Array()
  var currentDate = startDate;
  while (currentDate <= stopDate) {
      dateArray.push(new Date(currentDate))
      currentDate = addOneDay(currentDate)
  }
  return dateArray;
}

function handleWorkingTime(element) {
  element.start = new Date(element.start)
  element.end = new Date(element.end)
  element.startMoment = moment(element.start)
  element.endMoment = moment(element.end)

  let d = moment.duration(element.endMoment.diff(element.start))

  // use for graphs
  element.dates = getDates(element.start, element.end)
  element.weekStart = element.startMoment.isoWeek()
  element.dayStart = element.startMoment.isoWeekday()
  element.weekEnd = element.endMoment.isoWeek()
  element.dayEnd = element.endMoment.isoWeekday()
  element.offsetHours = d.asHours()
  element.monthStart = element.startMoment.month()
  element.monthEnd = element.endMoment.month()

  // user for workingtimes view
  element.startTxt = element.startMoment.locale('fr').format('dddd Do MMMM, HH:mm:ss, YYYY')
  element.endTxt = element.endMoment.locale('fr').format('dddd Do MMMM, HH:mm:ss, YYYY')
  element.offset =  d.days() + 'd ' + d.hours() + 'h ' + d.minutes() + 'm ' + d.seconds() + 's'
}

export default new Vuex.Store({
  state: {
    status: '',
    token: localStorage.getItem('token') || '',
    user: JSON.parse(localStorage.getItem('user')) || {},
    workingTimes: [],
    managerWorkingTimes: [],
    teamWorkingTimes: []
  },
  mutations: {
    auth_request(state){
      state.status = 'loading'
    },
    auth_success(state, data){
      state.status = 'success'
      state.token = data.token
      state.user = data.user
    },
    auth_error(state){
      state.status = 'error'
    },
    logout(state){
      state.status = ''
      state.token = ''
    },
    fill_workingtimes(state, data) {
      
      if(!data.forManager) {
        state.workingTimes = data.workingTimes
      } else {
        state.managerWorkingTimes = data.workingTimes
      }
    },
    clear_workingtimes(state, forManager) {
      if(!forManager){
        state.workingTimes = []
      } else {
        state.managerWorkingTimes = []
      }
    },
    fill_teamWorkingtimes(state, teamWorkingTimes) {
      state.teamWorkingTimes = teamWorkingTimes
    },
    clear_teamWorkingtimes(state) {
      state.teamWorkingTimes = []
    },
    update_email(state, email) {
      state.user.email = email
    }
  },
  actions: {
    updateEmail({commit}, email) {
      commit('update_email', email)
    },
    deleteUser({commit}){
      commit('logout')

      localStorage.removeItem('user')
      localStorage.removeItem('token')
      delete axios.defaults.headers.common['Authorization']
    },
    login({commit}, user){
      return new Promise((resolve, reject) => {
        let token

        commit('auth_request')
        axios({url: 'http://' + (process.env.VUE_APP_API_HOST || "0.0.0.0") + ':4000/api/users/sign_in', data: user, method: 'POST' })
        .then(resp => {
          /* get token */
          token = resp.data.jwt
          /* setup header */
          axios.defaults.headers.common['Authorization'] = 'Bearer ' + token
          localStorage.setItem('token', token)

          /* get user informations */
          axios({url: 'http://' + (process.env.VUE_APP_API_HOST || "0.0.0.0") + ':4000/api/users/my_user', method: 'GET' })
          .then(resp => {
            const myUser = resp.data
            localStorage.setItem('user', JSON.stringify(myUser))

            const data = {"user": myUser, "token": token}

            commit('auth_success', data)
            resolve(resp)
          })
          .catch(err => {
            reject(err)
          })
        })
        .catch(err => {
          commit('auth_error')
          localStorage.removeItem('token')
          reject(err)
        })
      })
    },
    register({commit}, user){
      return new Promise((resolve, reject) => {
        axios({url: 'http://' + (process.env.VUE_APP_API_HOST || "0.0.0.0") + ':4000/api/users/sign_up', data: {
          "user": user
        }, method: 'POST' })
        .then(resp => {
          resolve(resp)
        })
        .catch(err => {
          reject(err)
        })
      })
    },
    logout({commit}){
      return new Promise((resolve, reject) => {
        commit('logout')
        
        axios({url: 'http://' + (process.env.VUE_APP_API_HOST || "0.0.0.0") + ':4000/api/users/sign_out',data: null, method: 'POST' })
        .then(() => {
          localStorage.removeItem('token')
          localStorage.removeItem('user')
          delete axios.defaults.headers.common['Authorization']

          resolve()
        })
        .catch(err => {
          commit('auth_error', err)

          reject(err)
        })
      })
    },
    getWorkingTimes({commit}, data){
      return new Promise((resolve, reject) => {
        let workingTimes
        
        let id = data.id ? data.id : this.state.user.id

        commit('clear_workingtimes', data.forManager)
        axios.get('http://' + (process.env.VUE_APP_API_HOST || "0.0.0.0") + ':4000/api/workingtimes/' + id)
          .then(response => {
              if(response.status === 200) {
                  workingTimes = response.data.data

                  workingTimes.forEach(element => {
                      handleWorkingTime(element)
                  })

                  commit('fill_workingtimes', {workingTimes: workingTimes, forManager: data.forManager})
                  resolve()
              }
          }).catch(err => {
            commit('auth_error', err)
            reject(err)
          })
      })
    },
    getTeamWorkingTimes({commit}, teamId){
      return new Promise((resolve, reject) => {
        let teamWorkingTimes

        commit('clear_teamWorkingtimes')
        axios.get('http://' + (process.env.VUE_APP_API_HOST || "0.0.0.0") + ':4000/api/workingtimes/team/' + teamId)
          .then(response => {
              if(response.status === 200) {
                teamWorkingTimes = response.data.data

                teamWorkingTimes.forEach(element => {
                    handleWorkingTime(element)
                })

                commit('fill_teamWorkingtimes', teamWorkingTimes)
                resolve()
              }
          }).catch(err => {
            commit('auth_error', err)
            reject(err)
          })
      })
    }
  },
  getters : {
    isLoggedIn: state => !!state.token,
    authStatus: state => state.status,
    isAdmin: state => state.user.role ? state.user.role.id == ADMIN_ID : false,
    isManager: state => state.user.role ? state.user.role.id == MANAGER_ID : false,
    isEmployee: state => state.user.role ? state.user.role.id == EMPLOYEE_ID : false,
    user: state => state.user,
    workingTimes: state => state.workingTimes,
    managerWorkingTimes: state => state.managerWorkingTimes,
    teamWorkingTimes: state => state.teamWorkingTimes
  }
})
