import Vue from 'vue'
import App from './App.vue'
import Axios from 'axios'
import vuetify from './plugins/vuetify'
import {ADMIN_ID, MANAGER_ID, EMPLOYEE_ID} from './roles.js'
import store from './store.js'
import router from './router.js'
import moment from 'moment'

Vue.config.productionTip = false
Vue.prototype.$http = Axios

// Roles ids
Vue.prototype.$ADMIN_ID = ADMIN_ID
Vue.prototype.$MANAGER_ID = MANAGER_ID
Vue.prototype.$EMPLOYEE_ID = EMPLOYEE_ID

let token

if((token = localStorage.getItem('token'))){
  Axios.defaults.headers.common['Authorization'] = 'Bearer ' + token
}

moment.locale('fr', {
  week: {
      dow: 1 // Monday is the first day of the week.
  }
});

new Vue({
  router,
  vuetify,
  store,
  render: h => h(App)
}).$mount('#app')
