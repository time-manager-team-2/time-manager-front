export const teamDonutChartData = {
    type: 'doughnut',
    data: {
      // fill in js
      labels: [],
      datasets: [
        {
          // fill in js
          data: [],
          // fill in js
          backgroundColor: []
        }
      ]
    },
    options: {
      responsive: true,
      maintainAspectRatio: false,
      title: {
        display: true,
        text: 'monthly working hours (by employee)'
      }
    }
  }

  export default teamDonutChartData