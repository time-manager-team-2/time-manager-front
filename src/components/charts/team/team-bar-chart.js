export const teamBarChartData = {
    type: 'bar',
    data: {
      // fill in js
      labels: [],
      datasets: [
        {
            // fill in js
            backgroundColor: [],
            // fill in js
            data: []
        }
      ]
    },
    options: {
      responsive: true,
      maintainAspectRatio: false,
      legend: {
        display: false
      },
      title: {
        display: true,
        text: 'Average attendance rate (hours/day)'
      },
      scales: {
        yAxes: [{
          ticks: {
            beginAtZero: true
          }
        }]
      }
    }
  }

  export default teamBarChartData