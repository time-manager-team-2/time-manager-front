export const teamLineChartData = {
    type: 'line',
    data: {
        // fill in js
        labels: [],
        datasets: [{ // one line graph
                label: 'Work hours',
                // fill in js
                data: [],
                fill: false,
                backgroundColor: [
                    'rgba(255,0,0,.5)', // Blue
                    'rgba(255,0,0,.5)',
                    'rgba(255,0,0,.5)',
                    'rgba(255,0,0,.5)',
                    'rgba(255,0,0,.5)',
                    'rgba(255,0,0,.5)',
                    'rgba(255,0,0,.5)'
                ],
                borderColor: [
                    '#36495d',
                    '#36495d',
                    '#36495d',
                    '#36495d',
                    '#36495d',
                    '#36495d',
                    '#36495d'
                ],
                borderWidth: 3
            }
        ]
    },
    options: {
        responsive: true,
        maintainAspectRatio: false,
        lineTension: 1,
        legend: {
            display: false
        },
        title: {
            display: true,
            text: 'Total working hours for past month'
        },
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
}

export default teamLineChartData