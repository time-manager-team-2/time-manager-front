export const lineChartData = {
    type: 'line',
    data: {
        labels: ['Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche'],
        datasets: [{ // one line graph
                label: 'Arrival',
                // fill in js
                data: [],
                fill: false,
                backgroundColor: [
                    'rgba(255,0,0,.5)', // Blue
                    'rgba(255,0,0,.5)',
                    'rgba(255,0,0,.5)',
                    'rgba(255,0,0,.5)',
                    'rgba(255,0,0,.5)',
                    'rgba(255,0,0,.5)',
                    'rgba(255,0,0,.5)'
                ],
                borderColor: [
                    '#36495d',
                    '#36495d',
                    '#36495d',
                    '#36495d',
                    '#36495d',
                    '#36495d',
                    '#36495d'
                ],
                borderWidth: 3
            },
            { // one line graph
                label: 'Departure',
                // fill in js
                data: [],
                fill: "-1",
                backgroundColor: [
                    'rgba(255,255,0,.5)',
                    'rgba(255,255,0,.5)',
                    'rgba(255,255,0,.5)',
                    'rgba(255,255,0,.5)',
                    'rgba(255,255,0,.5)',
                    'rgba(255,255,0,.5)',
                    'rgba(255,255,0,.5)'
                ],
                borderColor: [
                    '#36495d',
                    '#36495d',
                    '#36495d',
                    '#36495d',
                    '#36495d',
                    '#36495d',
                    '#36495d'
                ],
                borderWidth: 3
            }
        ]
    },
    options: {
        responsive: true,
        maintainAspectRatio: false,
        lineTension: 1,
        scales: {
            yAxes: [{
                ticks: {
                    //beginAtZero: true,
                    //reverse: true
                }
            }]
        }
    }
}

export const managerLineChartData = JSON.parse(JSON.stringify(lineChartData))