export const donutChartData = {
    type: 'doughnut',
    data: {
      labels: ['Last week', 'Current week'],
      datasets: [
        {
          // fill in js
          data: [],
          backgroundColor: [
            'blue',
            'green'
          ]
        }
      ]
    },
    options: {
      responsive: true,
      maintainAspectRatio: false,
      title: {
        display: true,
        text: 'Hours worked'
      }
    }
  }

  export const managerDonutChartData = JSON.parse(JSON.stringify(donutChartData))