import Vue from 'vue'
import VueRouter from 'vue-router'
import { ADMIN_ID } from './roles.js'

import ChartManager from './components/ChartManager.vue'
import Register from './components/Register.vue'
import Login from './components/Login.vue'
import WorkingTime from './components/WorkingTime.vue'
import WorkingTimes from './components/WorkingTimes.vue'
import Dashboard from './components/Dashboard.vue'
import Promote from './components/Promote.vue'
import EditUser from './components/EditUser.vue'

Vue.use(VueRouter)

const router = new VueRouter({
    routes: [
      {
        path: '/login',
        name: 'login',
        component: Login,
        meta: { 
            guest: true
        }
      },
      {
        path: '/register',
        name: 'register',
        component: Register,
        meta: { 
            guest: true
        }
      },
      {
        path: '/chartManager',
        name: 'chartmanager',
        component: ChartManager,
        meta: { 
            requiresAuth: true
        }
      },
      {
        path: '/workingTime',
        name: 'workingtime',
        component: WorkingTime,
        meta: { 
            requiresAuth: true
        }
      },
      {
        path: '/editUser',
        name: 'edituser',
        component: EditUser,
        meta: { 
            requiresAuth: true
        }
      },
      {
        path: '/workingTime',
        name: 'workingtimeTwo',
        component: WorkingTime,
        props: true,
        meta: { 
            requiresAuth: true
        }
      },
      {
        path: '/workingTimes',
        name: 'workingtimes',
        component: WorkingTimes,
        meta: { 
            requiresAuth: true
        }
      },
      {
        path: '/',
        name: 'dashboard',
        component: Dashboard,
        meta: { 
          requiresAuth: true
        }
      },
      {
        path: '/Promote',
        name: 'promote',
        component: Promote,
        meta: { 
          requiresAuth: true,
          is_admin : true
        }
      }
    ]
})

router.beforeEach((to, from, next) => {
    if(to.matched.some(record => record.meta.requiresAuth)) {
        if (localStorage.getItem('token') == null) {
            next({
                path: '/login',
                params: { nextUrl: to.fullPath }
            })
        } else {
            let user = JSON.parse(localStorage.getItem('user'))
            if(to.matched.some(record => record.meta.is_admin)) {
                if(user.role.id == ADMIN_ID){
                    next()
                }
                else{
                    next({ name: 'dashboard'})
                }
            }else {
                next()
            }
        }
    } else if(to.matched.some(record => record.meta.guest)) {      
        if(localStorage.getItem('token') == null){
            next()
        }
        else{
            next({ path: '/'})
        }
    }else {
        next() 
    }
})

export default router